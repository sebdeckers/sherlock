const { join } = require('path')
const sherlock = require('..')
const assert = require('assert')

const fixtures = [
  {
    folder: 'css',
    expected: [
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/string-double-quotes.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/string-single-quotes.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/url-no-quotes.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/url-double-quotes.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/url-single-quotes.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/simple-media-query.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/two-media-queries.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/complex-media-query.css')
      },
      {
        type: 'font/*',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/sansation_light.woff')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/single-url.gif')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/first-url.gif')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/second-url.gif')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/css/style.css'),
        path: join(__dirname, 'fixtures/css/multiple-values.gif')
      }
    ]
  },
  {
    folder: 'js',
    expected: [
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/js/script.js'),
        path: join(__dirname, 'fixtures/js/choreographer.js') },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/js/script.js'),
        path: join(__dirname, 'fixtures/js/components/cli-help.js') },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/js/script.js'),
        path: join(__dirname, 'fixtures/js/abc.js') },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/js/script.js'),
        path: join(__dirname, 'fixtures/js/foo.js') },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/js/script.js'),
        path: join(__dirname, 'fixtures/js/bar.js') } ]
  },
  {
    folder: 'html',
    expected: [
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/relative/stylesheet.css')
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/absolute/stylesheet.css')
      },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/absolute/javascript.js')
      },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/relative/javascript.js')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/absolute/image.jpg')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/relative/image.jpg')
      },
      {
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/lazy/loading/image.jpg'),
        type: 'image/*',
        loading: 'lazy'
      },
      {
        parent: join(__dirname, 'fixtures/html/index.html'),
        path: join(__dirname, 'fixtures/html/eager/loading/image.jpg'),
        type: 'image/*',
        loading: 'eager'
      }
    ]
  },
  {
    folder: 'external',
    expected: [
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'https://example.net/stylesheet.css'
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'http://example.net/stylesheet.css'
      },
      {
        type: 'text/css',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: '//example.net/stylesheet.css'
      },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'https://example.net/script.js'
      },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'http://example.net/script.js'
      },
      {
        type: 'application/javascript',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: '//example.net/script.js'
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'https://example.net/image.jpg'
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: 'http://example.net/image.jpg'
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/external/index.html'),
        path: '//example.net/image.jpg'
      }
    ]
  },
  {
    folder: 'query-hash',
    expected: [
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/query-hash/index.html'),
        path: join(__dirname, '/fixtures/query-hash/query/image.svg?query')
      },
      {
        type: 'image/*',
        parent: join(__dirname, 'fixtures/query-hash/index.html'),
        path: join(__dirname, '/fixtures/query-hash/hash/image.svg#hash')
      }
    ]
  }
]

for (const fixture of fixtures) {
  const dir = join(__dirname, 'fixtures', fixture.folder)
  sherlock(dir).then((files) => {
    if (!Array.isArray(files)) {
      throw new Error('Must be an array')
    }

    for (const file of files) {
      const properties = ['path', 'parent', 'type']
      for (const property of properties) {
        if (file[property] === undefined) {
          throw new Error(`Missing property: ${property}`)
        }
      }
    }

    assert.deepStrictEqual(files, fixture.expected)
  })
}
